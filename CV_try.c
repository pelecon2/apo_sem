/*******************************************************************
  Project main function template for MicroZed based MZ_APO board
  designed by Petr Porazil at PiKRON

  change_me.c      - main file

  include your name there and license for distribution.

  Remove next text: This line should not appear in submitted
  work and project name should be change to match real application.
  If this text is there I want 10 points subtracted from final
  evaluation.

 *******************************************************************/

#define _POSIX_C_SOURCE 200112L

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "serialize_lock.h"

// enum na fb errors

#define FBUF_DTYPE uint32_t;

#define FB_BLUE_MAX 0x1f
#define FB_GREEN_MAX 0x3f
#define FB_RED_MAX 0x1f

#define RGB(red, green, bule) (((red) & 0x1f) << 11 | ((green) & 0x3f) << 5 | ((blue) & 0x1f))
#define R(color) (((color) >> 11) & 0x1f)
#define G(color) (((color) >> 5) & 0x3f)
#define B(color) (((color) >> 0) & 0x1f)

#define FB_BLUE (RGB(0,0,FB_BLUE_MAX))
...

typedef struct frame_buffer {
  FBUF_DTYPE *buffer;
  uint32_t width,heigth;
  void (*draw)(frame_buffer_t* fbuf, int color)
} frame_buffer_t;

int32_t isInits(){

}

int32_t frame_buffer_init(frame_buffer_t* fbuf, FBUF_DTYPE h,FBUF_DTYPE w){
  if(w < 0 | h < 0){
    //blbost
  }

  int size = 0; // size
  fbuf.buffer = malloc(size);

  for(int i=0;i<size;++i){
    // init to smth
  }

  frame_buffer.width = w;
  frame_buffer.heigth = h;

  return // success or error
}

int32_t draw_fram_buffer(frame_buffer_t* fbuf, color){
  for(size){

  }
}

int main(int argc, char *argv[])
{

  /* Serialize execution of applications */

  /* Try to acquire lock the first */
  if (serialize_lock(1) <= 0) {
    printf("System is occupied\n");

    if (1) {
      printf("Waitting\n");
      /* Wait till application holding lock releases it or exits */
      serialize_lock(0);
    }
  }

  volatile void *lcdBaseAddr = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
  if(lcdBaseAddr == NULL){
    printf("Counld not get rgb");
    exit(1);
  }

  parlcd_hx8357_init(lcdBaseAddr);

  *(volatile uint16_t *)(lcdBaseAddr + PARLCD_REG_CMD_o) = 0x2c;
  for (int y=0;y<320;++y){
    for(int x=0;x<240;++x){
      *(volatile uint32_t *)(lcdBaseAddr + PARLCD_REG_DATA_o) = 0x00BB;
    }
  }

  

  printf("Hello world\n");

  sleep(4);

  printf("Goodbye world\n");

  /* Release the lock */
  serialize_unlock();

  return 0;
}
