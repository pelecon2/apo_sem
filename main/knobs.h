/*******************************************************************
    Header for knobs.c
    Class for accessing perifers knobs and knob buttons
    authors: Pelech Ondrej, Pham Thi Thien Trang 
    date:  Mai 2023
 *******************************************************************/

#ifndef __KNOBS_H__
#define __KNOBS_H__

#include <stdint.h>
#include <time.h>

#include "player.h"
#include "projectile.h"
#include "enemy.h"
#include "draw.h"
#include "obstacle.h"

//returns pointer to location of knob trun value on MZ_APO
uint32_t knobs(void* spilled_base);

//returns pointer to location of knob press value on MZ_APO
uint32_t buttons(void* spilled_base);

void handle_knob_movement(uint32_t knob_value, uint32_t prev_knob_value, uint32_t* prev_red, uint32_t* prev_blue,
                         uint32_t* prev_green, player_t* player, frame_buffer_t* frame_buffer);

void handle_red_knob_movement(uint32_t red_knob, uint32_t prev_red, player_t* player, frame_buffer_t* frame_buffer);

void handle_green_knob_movement(uint32_t green_knob, uint32_t prev_green, player_t* player, frame_buffer_t* frame_buffer);

void handle_blue_knob_movement(uint32_t blue_knob, uint32_t prev_blue, player_t* player);

void handle_knob_buttons(uint32_t knob_state, player_t* player, frame_buffer_t* frame_buffer, projectile_t** projectiles,
                        int* num_projectiles, int max_projectiles, void* spiled_base);


void handle_immunity_button(uint32_t knob_state, int* button_b_pressed, player_t* player, void* spiled_base,
                            time_t* immunity_start_time, int* can_immunity);

void handle_dash_button(uint32_t knob_state, int* button_g_pressed, player_t* player, frame_buffer_t* frame_buffer,
                                                void* spiled_base, time_t* dash_start_time, int* can_dash);

void handle_shoot_button(uint32_t knob_states, int* button_r_pressed, player_t* player, frame_buffer_t* frame_buffer,
                        projectile_t** projectiles, int* num_projectiles, int max_projectiles, void* spiled_base);



void handle_button_released(uint32_t knob_states, int* button_r_pressed, int* button_g_pressed, int* button_b_pressed);

#endif
