/*******************************************************************
    Header for collision.c
    File representing collisions between objects
    authors: Pelech Ondrej, Pham Thi Thien Trang 
    date:  May 2023
 *******************************************************************/

#ifndef __COLLISION_H__
#define __COLLISION_H__

#include <stdint.h>

#include "enemy.h"
#include "player.h"
#include "projectile.h"
#include "obstacle.h"

// checks for object collisions of exact types
void check_collisions(player_t* player, enemy_t** enemies, obstacle_t** obstacles, int num_obstacles, int max_obstacles,
                    projectile_t** projectiles, int num_projectiles, int max_projectiles, int num_enemy);


// checks for collisions with projectiles
void check_collision_with_projectiles(player_t* player, enemy_t** enemies, obstacle_t** obstacles, int num_obstacles, int max_obstacles, projectile_t** projectiles,
                    int num_projectiles, int max_projectiles, int num_enemy);

// checks for collisions with obstacles
void check_player_collision_with_obstacles(player_t* player, obstacle_t** obstacles, int num_obstacles, int max_obstacles);

#endif 