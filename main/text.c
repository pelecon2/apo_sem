#include <stdlib.h>
#include "text.h"
#include "utils.h"
#include "draw.h"

text_t* get_text(const char* text){
    text_t* self = my_malloc(sizeof(text_t));

    self->text = text;

    self->draw = text_draw;
    self->free = free_text;

    return self;
}

int16_t text_draw(struct text* self,struct frame_buffer* buffer, int16_t x, int16_t y){
    int16_t ret = EXIT_SUCCESS;
    int16_t text_len = 0;
    if(self == NULL){
        ret = TEXT_NULL;
    } else {
        int16_t i=0;
        while(self->text[i] != '\0'){
            i++;
        }

        text_len = i;

        if(x < 0 || text_len * 8*3 + x > SCREEN_WIDTH || y < 0 || y + 16 > SCREEN_HEIGHT){
            ret = TEXT_LOCATION;
        }

    }

    if(ret == EXIT_SUCCESS){
        for(int i=0;i<text_len;++i){
            int width = 8*3;

            buffer->draw_letter(buffer,x+i*width,y,self->text[i]);
        }
    }

    return ret;
}



void free_text(struct text* self){
    free(self->text);
    free(self);
}
