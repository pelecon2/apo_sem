/*******************************************************************
    Header for led_line.c
    Class for accessing led_line
    authors: Pelech Ondrej, Pham Thi Thien Trang 
    date:  Mai 2023
*******************************************************************/
#ifndef LED_LINE_H
#define LED_LINE

#include <stdint.h>

/* Set LED line to corresponding number */
void set_led1_on(void* spiled_base, uint32_t num);

/* Take used bullet and shift the bits to left */
void set_ammo_count(void* spiled_base, uint32_t ammo_used);

#endif
