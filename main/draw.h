/*******************************************************************
    Header for draw.c
    Method representing frame_buffer
    authors: Pelech Ondrej, Pham Thi Thien Trang 
    date:  Mai 2023
 *******************************************************************/

#ifndef __DRAW_H__
#define __DRAW_H__

#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif

#ifndef __SCREEN_DIMENSIONS__
#define __SCREEN_DIMENSIONS__

#define SCREEN_WIDTH 480
#define SCREEN_HEIGHT 320

#endif

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "serialize_lock.h"

#include "utils.h"

// F_BUFF errors
enum {ERROR_INIT = 101,ERROR_WRONG_INIT_INPUT, ERROR_ADDRESS_ACCESS, ERROR_BUFFER_NULL};

// F_BUFF errors
enum {ERROR_BUF_INIT = 201,ERROR_INVALID_VALUES};

#define FBUF_DTYPE uint16_t

#define FB_BLUE_MAX 0x1f
#define FB_GREEN_MAX 0x3f
#define FB_RED_MAX 0x1f

#define RGB(red, green, blue) (((red) & 0x1f) << 11 | ((green) & 0x3f) << 5 | ((blue) & 0x1f))
#define R(color) (((color) >> 11) & 0x1f)
#define G(color) (((color) >> 5) & 0x3f)
#define B(color) (((color) >> 0) & 0x1f)

#define FB_BLUE (RGB(0, 0, FB_BLUE_MAX))
#define FB_GREEN (RGB(0, FB_GREEN_MAX, 0))
#define FB_RED (RGB(FB_RED_MAX, 0, 0))
#define FB_BLACK (RGB(0,0,0))
#define FB_WHITE (RGB(FB_RED_MAX, FB_GREEN_MAX, FB_BLUE_MAX)

#define DEFAULT_PICTURE_SIZE 16

#define INVERT_COLORS 0 // set to not 0

typedef struct frame_buffer{
    FBUF_DTYPE *buffer;
    uint32_t width, height;
    uint32_t buffer_length;
    uint32_t mem_size;

    void* background_color;

    volatile void *lcdBaseAddr;

    // draws buffer to MZ
    // Method draw
    int16_t (*draw)(struct frame_buffer* self);

    // draws plane to buffer to pos x, y
    // Method draw_plane
    int16_t (*draw_plane)(struct frame_buffer* self, const int x, const int y);

    // draws obstacle to buffer to pos x, y
    // Method draw_obstacle
    int16_t (*draw_obstacle)(struct frame_buffer* self, const int x, const int y);

    // draws obstacle to buffer to pos x, y
    // Method draw_enemy
    int16_t (*draw_enemy)(struct frame_buffer* self, const int x, const int y);

    // draws projectile of type to buffer to pos x,y 
    // Method draw_projectile
    int16_t (*draw_projectile)(struct frame_buffer* self, const int x, const int y, uint16_t type);

    // draws letter to buffer pos x,y
    // Method draw_letter
    int16_t (*draw_letter)(struct frame_buffer* self, const int x, const int y, char letter);

    // set whole buffer to color 
    // Method color_background
    int16_t (*color_background)(struct frame_buffer* self,uint16_t color);

    // Method mull_buffer
    int16_t (*null_buffer)(struct frame_buffer* self);
    
    // frees buffer
    void (*free)(struct frame_buffer* self);

} frame_buffer_t;

/**
* inits buffer with entered paramets
* input param frame_buffer, int location x, int location y
* returs EXIT_SUCCES on succes BAFFER_ERROR otherwise
**/
int16_t frame_buffer_init(struct frame_buffer* self, FBUF_DTYPE h, FBUF_DTYPE w);

/**
* draws frame_buffer to screen
* input param frame_buffer, int location x, int location y
* returs EXIT_SUCCES on succes BAFFER_ERROR otherwise
**/
int16_t draw(struct frame_buffer* self);

/**
* draws plane aka player to frame_buffer to location x,y
* input param frame_buffer, int location x, int location y
* returs EXIT_SUCCES on succes BAFFER_ERROR otherwise
**/
int16_t draw_plane(struct frame_buffer* self, const int x, const int y);


/**
* draws obstacle to frame_buffer to location x,y
* input param frame_buffer, int location x, int location y
* returs EXIT_SUCCES on succes BAFFER_ERROR otherwise
**/
int16_t draw_obstacle(struct frame_buffer* self, const int x, const int y);

/**
* draws enemy to frame_buffer to location x,y
* input param frame_buffer, int location x, int location y
* returs EXIT_SUCCES on succes BAFFER_ERROR otherwise
**/
int16_t draw_enemy(struct frame_buffer* self, const int x, const int y);

/**
* draws projectile to frame_buffer to location x,y
* input param frame_buffer, int location x, int location y, PROJECTILE_TYPE type
* returs EXIT_SUCCES on succes BAFFER_ERROR otherwise
**/
int16_t draw_projectile(struct frame_buffer* self, const int x, const int y, uint16_t type);

/**
* draws letter to frame_buffer to location x,y
* input param frame_buffer, int location x, int location y, char
* returs EXIT_SUCCES on succes BAFFER_ERROR otherwise
**/
int16_t draw_letter(struct frame_buffer* self, const int x, const int y, char letter);

/**
* set whole frame_buffer to color overriteing EVERYTHING
* input param frame_buffer, uint16_t color
* returs EXIT_SUCCES on succes BAFFER_ERROR otherwise
**/
int16_t color_background(struct frame_buffer* self,uint16_t color);

/** 
* set whole frame_buffer to default background color overriteing EVERYTHING
* input param frame_buffer
* returs EXIT_SUCCES on succes BAFFER_ERROR otherwise
**/
int16_t null_buffer(struct frame_buffer* self);

/** 
* mutlipiles buffer by multiplicator of square dimensions
* ret_buffer needs to be pre allocated
* input param ret_buffer - destination, buffer - to be inflated, pictue_size - size of one side, multiplicator - constant multiplaing the buffer
**/
void multiply_buffer(uint16_t ret_buffer[],uint16_t buffer[],const uint16_t picture_size,uint16_t multipliactor);

/** 
* mutlipiles buffer by multiplicator of square dimensions
* ret_buffer needs to be pre allocated
* input param ret_buffer - destination, buffer - to be inflated, width - width of buffer, height - height of buffer, multiplicator - constant multiplaing the buffer
**/
void multiply_letter(uint16_t ret_buffer[],uint16_t buffer[],const uint16_t width, const uint16_t height,uint16_t multipliactor);

/**
* frees buffer struct
**/
void free_buffer(struct frame_buffer* self);

#endif
