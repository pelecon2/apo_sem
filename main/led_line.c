/*******************************************************************
    Class for accessing led_line
    authors: Pelech Ondrej, Pham Thi Thien Trang 
    date:  Mai 2023
 *******************************************************************/
#include <stdint.h>

#include "mzapo_regs.h"
#include "led_line.h"

// shoes num on led line
void set_led1_on(void* spiled_base, uint32_t num) {
    uint32_t *line = (uint32_t *)(spiled_base + SPILED_REG_LED_LINE_o);
    *line = num;
}

// displies ammo count on led_line
void set_ammo_count(void* spiled_base, uint32_t ammo_used) {
    uint32_t bits_to_turn_on = 0xFFFFFFFF << ammo_used;
    set_led1_on(spiled_base, bits_to_turn_on);
}
