#ifndef __LETTERS_H__
#define __LETTERS_H__

#ifndef __TEXT_TYPES__
#define __TEXT_TYPES__

#define text_LABEL 0
#define text_TEXT 1

#endif
#endif

int16_t* get_letter(char letter);
