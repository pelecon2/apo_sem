/*******************************************************************
    Header for utils.c
    File filled with utility methods
    authors: Pelech Ondrej, Pham Thi Thien Trang 
    date:  Mai 2023
 *******************************************************************/

#ifndef __UTILS_H__
#define __UTILS_H__

#include <stdlib.h>
#include <stdbool.h>

#define INF 0Xffffffff
#define INF_SHORT 0xffff

#define LOG_LEVEL_DEBUG 0
#define LOG_LEVEL_INFO 1
#define LOG_LEVEL_ERROR 2

#define LOG_LEVEL LOG_LEVEL_ERROR

// prints info messagse in format "INFO:'msg'"
void info(const char *str);

// prints error messagse to stderr in format "ERROR:'msg'"
void error(const char *str);

// prints debug messagse to stderr in format "DEBUG:'msg'"
void debug(const char *str);

// prints warning messagse to stderr in format "WARN:'msg'"
void warn(const char *str);

// prints str with a new line to stdout
void println(const char *str);

//prints error quote and exits program is r is false
void my_assert(bool r, const char *fcname, const int line, const char *flname);

// allocates mem of the input size
// takes care of malloc failiure by exiting program
void* my_malloc(const size_t size);

// returns bigger value of inputed nunmbers
long max(long number1, long number2);

// returns smaller value of inputed numbers
long min(long number1, long number2);
#endif
