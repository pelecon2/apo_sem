/*******************************************************************
    Header for collision_rect.c
    File representing collision rectangle
    authors: Pelech Ondrej, Pham Thi Thien Trang 
    date:  May 2023
 *******************************************************************/

#ifndef __COLLISION_RECT_H__
#define __COLLISION_RECT_H__

#include <stdint.h>
#include <stdbool.h>

typedef struct collision_rect{
    int16_t x;
    int16_t y;
    int16_t width;
    int16_t height;
} collision_rect_t;

// checks for object collisions
bool check_collision_rect(collision_rect_t* rect1, collision_rect_t* rect2);

#endif 