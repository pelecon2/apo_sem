/*******************************************************************
  File filled with utility methods
  authors: Pelech Ondrej, Pham Thi Thien Trang 
  date:  Mai 2023
 *******************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>

#include "utils.h"

// prints info messagse in format "INFO:'msg'"
void info(const char *str) {
    if(LOG_LEVEL <= LOG_LEVEL_INFO){
        fprintf(stderr, "INFO: %s\n", str);
    }
    return;
}

// prints error messagse in format "ERROR:'msg'"
void error(const char *str) {
    if(LOG_LEVEL <= LOG_LEVEL_ERROR){
        fprintf(stderr, "ERROR: %s\n", str);
    }
    return;
}

// prints debug messagse in format "DEBUG:'msg'"
void debug(const char *str) {
    if(LOG_LEVEL <= LOG_LEVEL_DEBUG){
        fprintf(stderr, "DEBUG: %s\n", str);
    }
    return;
}

// prints warning messagse in format "WARN:'msg'"
void warn(const char *str) {
    fprintf(stderr, "WARN: %s\n", str);
    return;
}


// prints str with a new line to stdout
void println(const char *str){
    fprintf(stdout,"%s\n",str);
}


void my_assert(bool r, const char *fcname, int line, const char *flname) {
    if (!r) {
        fprintf(stderr, "ERROR:my_assert failed FAIL: %s() line %d in %s\n", fcname, line, flname);
        exit(105);
    }
    return;
}

void* my_malloc(size_t size) {
    void *t = malloc(size);
    if (!t) {
        error("my_malloc failed");
        exit(101);
    }
    return t;
}

long max(long number1, long number2) {
    return number1 > number2 ? number1 : number2;
}

// returns smaller value of inputed numbers
long min(long number1, long number2) {
    return number1 < number2 ? number1 : number2;
}
