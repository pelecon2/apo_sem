/*******************************************************************
  File respresenting projeciles
  authors: Pelech Ondrej, Pham Thi Thien Trang 
  date:  May 2023
 *******************************************************************/

#include <stdlib.h>
#include "draw.h"
#include "utils.h"

#include "projectile.h"
#include "collision_rect.h"

projectile_t* get_projectile(char plr,int16_t x, int16_t y, int16_t type){
    projectile_t* self = my_malloc(sizeof(projectile_t));
    self->owner = NO_OWNER;

    self->init = projectile_init;
    self->set_location = projectile_set_location;
    self->draw = projectile_draw;
    self->update_location = projectile_update_location;
    self->free = free_projectile;

    if(self->init(self,plr,x,y,type) != EXIT_SUCCESS){
        free(self);
        self = NULL;
    }

    return self;
}

void free_projectile(struct projectile* self) {
    free(self->collision_rect);
    free(self);
}

int16_t projectile_init(struct projectile* self,char plr,int16_t x, int16_t y, int16_t type){
    int16_t ret = EXIT_SUCCESS;

    if(x < 0 || y < 0 || x > 480 || y > 360){
        ret = INVALID_LOCATION;
    } else if (plr != ENEMY && plr != PLAYER){
        error("invalid projectile owner");
        ret = INVALID_OWNER;
    } else if (type != PROJECTILE_SMALL && type != PROJECTILE_MEDIUM && type != PROJECTILE_BIG){
        error("wrong_projectile_type");
        ret = INVALID_PROJECTILE_TYPE;
    } else {
        self->owner = plr;
        self->x = x;
        self->y = y;
        self->type = type;

        // Initialize collision_rect
        self->collision_rect = my_malloc(sizeof(collision_rect_t));
        self->collision_rect->x = x;
        self->collision_rect->y = y;
        self->collision_rect->width = PROJECTILE_SIZE * type;
        self->collision_rect->height = PROJECTILE_SIZE * type;
    }

    return ret;
}

int16_t projectile_set_location(struct projectile* self,int16_t x,int16_t y){
    int16_t ret = EXIT_SUCCESS;

    if(x < 0 || y < 0 || x > 480 || y > 360){
        ret = INVALID_LOCATION;
    }

    self->x = x;
    self->y = y;
    self->collision_rect->x = x;
    self->collision_rect->y = y;

    return ret;
}

int16_t projectile_draw(struct projectile* self,struct frame_buffer* buffer){
    int16_t ret = EXIT_SUCCESS;

    if(!self || !buffer){
        ret = INVALID_INPUT;
    } else {
        buffer->draw_projectile(buffer, self->x, self->y, self->type);
    }
    return ret;
}


int16_t projectile_update_location(struct projectile* self,struct frame_buffer* buffer){
    int16_t ret = EXIT_SUCCESS;
    if(self->owner == PLAYER){
        self->set_location(self, self->x, self->y - PROJECTILE_DISTANCE_INCREMENT);
    } else if (self->owner == ENEMY) {
        self->set_location(self,  self->x, self->y + PROJECTILE_DISTANCE_INCREMENT);
    } else {
        // no owner projectile
        error("projecile with no onwer detected");
    }

    if(self->y < 0 || self->y > SCREEN_HEIGHT - self->collision_rect->height){
        ret = SCREEN_END;
        self->free(self); 
    } else {
        self->draw(self,buffer);
    }
    return ret;
}

int16_t sort_projectiles(projectile_t** projectiles, const int16_t last_projectile_index){
    int16_t idx = 0;
    for(int16_t i;i < last_projectile_index;++i){
        while(projectiles[idx] != NULL){
            if(idx >= last_projectile_index) {
                return idx;
            }
            ++idx;
        }

        if(projectiles[i] != NULL && i > idx){
            projectiles[idx] = projectiles[i];
            projectiles[i] = NULL; 
        }
    }
    
    return idx;
}