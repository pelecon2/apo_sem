#ifndef LED_RGB_H
#define LED_RGB

#include <stdint.h>
#include "player.h"

// sets color on rgb LED 1
void LED_rgb1(void* spiled_base, uint32_t color);

// sets color on rgb LED 2
void LED_rgb2(void* spiled_base, uint32_t color);

// returns the current rgb colour on LED 2
uint32_t get_led_rgb2_color(void* spiled_base);

// shows health update on rgb LED 1
void update_health_rgb1(void* spiled_base, int max_hp, int hp);

void change_led_rgb2_based_on_powerups(void* spiled_base, uint32_t current_time, uint32_t dash_start_time, uint32_t immunity_start_time, int* can_dash, int* can_dodge);

void turn_off_dash_color(void* spiled_base);

void turn_off_immunity_color(void* spiled_base);

#endif
