/*******************************************************************
  Header for player.c
  File representing player plane
  authors: Pelech Ondrej, Pham Thi Thien Trang 
  date:  May 2023
 *******************************************************************/

#include <stdlib.h>
#include "draw.h"
#include "utils.h"
#include "projectile.h"
#include "collision_rect.h"

#include "player.h"


player_t* get_player(int16_t x, int16_t y, int16_t hp){
    player_t* self = my_malloc(sizeof(player_t));

    self->init = player_init;
    self->draw = player_draw;
    self->update_location = player_update_location;
    self->set_location = player_set_location;
    self->shoot = player_shoot;
    self->dash = dash;
    self->damage = damage;
    self->imunity = imunity;
    self->projectile_scale_up = projectile_scale_up;
    self->projectile_scale_down = projectile_scale_down;
    self->heal = player_heal;
    self->add_ammo = player_add_ammo;
    self->free = free_player;

    self->init(self,x,y,hp);

    return self;
}

int16_t player_init(struct player* self,int16_t x, int16_t y, int16_t hp){
    int16_t ret = EXIT_SUCCESS;

    
    if (self == NULL){
        error("self in player init is NULL");
    } else {
        self->x = x;
        self->y = y;
        self->hp = hp;
        self->max_hp = hp;

        self->ammo = 100;
        self->cd = 0;

        self->max_ammo = 100; //percentage based
        self->max_cd = 100;
        self->projectile_type = PROJECTILE_SMALL;

        self->dodge_count = 0;

        // Initialize collision_rect
        self->collision_rect = my_malloc(sizeof(collision_rect_t));
        self->collision_rect->x = x;
        self->collision_rect->y = y;
        self->collision_rect->width = DEFAULT_PICTURE_SIZE * 3;
        self->collision_rect->height = DEFAULT_PICTURE_SIZE * 3;
    }

    return ret;
}

int16_t player_set_location(struct player* self,int16_t x,int16_t y){
    int16_t ret = EXIT_SUCCESS;
    
    if (self == NULL){
        ret = PLAYER_NULL;
        error("self in player_set_location is NULL");
    } else {
        self->x = x;
        self->y = y;
        
        self->collision_rect->x = x;
        self->collision_rect->y = y;
    }

    return ret;
}

int16_t player_draw(struct player* self,struct frame_buffer* buffer){
    int16_t ret = EXIT_SUCCESS;
    
    if (self == NULL){
        ret = PLAYER_NULL;
        error("player null in player draw");
    } else if (buffer == NULL){
        error("buffer null in player draw");
    } else {
        buffer->draw_plane(buffer, self->x, self->y);
    }

    return ret;
}

int16_t player_update_location(struct player* self,struct frame_buffer* buffer, int x, int y){
    int16_t ret = EXIT_SUCCESS;
    
    // set boundary
    if (y < DEFAULT_PICTURE_SIZE * 3 * 3) {
        y = DEFAULT_PICTURE_SIZE * 3 * 3;
    } else if (y > SCREEN_HEIGHT - DEFAULT_PICTURE_SIZE*3) {
        y = SCREEN_HEIGHT - DEFAULT_PICTURE_SIZE*3;
    } else if ( x > SCREEN_WIDTH - DEFAULT_PICTURE_SIZE*3) {
        x = SCREEN_WIDTH - DEFAULT_PICTURE_SIZE*3;
    } else if ( x < 0) {
        x = 0;
    }

    if (self->set_location(self,x,y) == EXIT_SUCCESS){
        ret = self->draw(self,buffer);
        
    } else {
        ret = EXIT_FAILURE;
    }
    return ret;
}

void free_player(struct player* self){
    free(self->collision_rect);
    free(self);
}

projectile_t* player_shoot(struct player* self,struct frame_buffer* buffer, int16_t projectile_type){
    projectile_t* projectile = NULL;
    if (self == NULL){
        error("player null in player draw");
    } else if (buffer == NULL){
        error("buffer null in player draw");
    } else {
        if (projectile_type == PROJECTILE_SMALL && self->ammo >= PROJECTILE_SMALL_COST){
            self->ammo -= PROJECTILE_SMALL_COST;
        } else if (projectile_type == PROJECTILE_MEDIUM && self->ammo >= PROJECTILE_MEDIUM_COST){
            self->ammo -= PROJECTILE_MEDIUM_COST;
        } else if (projectile_type == PROJECTILE_BIG && self->ammo >= PROJECTILE_BIG_COST){
            self->ammo -= PROJECTILE_BIG_COST;
        } else {
            info("player does not have enough ammo or projectile types do not match");
            return NULL;
        }
        
        projectile = get_projectile(PLAYER, self->x + DEFAULT_PICTURE_SIZE * 3 / 2 - PROJECTILE_SIZE * projectile_type/2, self->y - PROJECTILE_SIZE * projectile_type, projectile_type);
    }

    return projectile;
}

int16_t dash(struct player* self,struct frame_buffer* buffer, uint16_t direction){
    int16_t ret = EXIT_SUCCESS;

    if(self == NULL){
        error("player null in player draw");
        ret = PLAYER_NULL;
    } else if (buffer == NULL){
        error("buffer null in player draw");
    } else {
        if (USE_UNIVERSAL == true){
            if (direction == RIGHT){
                self->x += DASH_UNIVERSAL;
                self->collision_rect->x = self->x;
            } else if (direction == LEFT){
                self->x -= DASH_UNIVERSAL;
                self->collision_rect->x = self->x;
            } else {
                warn("not implemented direction of DODGE");
            }
        } else {
            warn("sage of non universal dash is not implemented");
        }
    }

    return ret;
}

int16_t damage(struct player* self){
    int16_t ret = EXIT_SUCCESS;

    if (self == NULL){
        error("player null in player draw");
        ret = PLAYER_NULL;
    } else {
        if (self->dodge_count > 0){
            self->dodge_count--;
        } else if (self->hp > 0) {
            self->hp--;
        }
    }

    return ret;
}

int16_t imunity(struct player* self){
    int16_t ret = EXIT_SUCCESS;

    if (self == NULL){
        error("player null in player draw");
        ret = PLAYER_NULL;
    } else {
        if (self->dodge_count + DODGE_INCREMENT <= MAX_DOGES){
            self->dodge_count += DODGE_INCREMENT;
        } else {
            info("dodges with increment would be over maximum - not adding more doges");
        }
    }

    return ret;
}

void projectile_scale_up(struct player* self) {
    self->projectile_type++;
    if (self->projectile_type > 3) {
        self->projectile_type = PROJECTILE_SMALL;
    }
}

void projectile_scale_down(struct player* self) {
    self->projectile_type--;
    if (self->projectile_type < 1) {
        self->projectile_type = PROJECTILE_BIG;
    }
}

void player_heal(struct player* self, int16_t heal) {
    self->hp += heal;
    if (self->hp > self->max_hp) {
        self->hp = self->max_hp;
    }
}

void player_add_ammo(struct player* self, int16_t ammo) {
    self->ammo += ammo;
    if (self->ammo > self->max_ammo) {
        self->ammo = self->max_ammo;
    }
}