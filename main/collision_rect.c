/*******************************************************************
    File representing collision rectangle
    authors: Pelech Ondrej, Pham Thi Thien Trang 
    date:  May 2023
 *******************************************************************/

#include <stdint.h>
#include <stdbool.h>

#include "collision_rect.h"


bool check_collision_rect(collision_rect_t* rect1, collision_rect_t* rect2) {
    bool overlap_x = rect1->x < rect2->x + rect2->width && rect1->x + rect1->width > rect2->x;
    bool overlap_y = rect1->y < rect2->y + rect2->height && rect1->y + rect1->height > rect2->y;
    return overlap_x && overlap_y;
}
