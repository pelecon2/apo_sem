/*******************************************************************
  Header for player.c
  File representing obstacle
  authors: Pelech Ondrej, Pham Thi Thien Trang 
  date:  May 2023
 *******************************************************************/

#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include "draw.h"
#include "utils.h"

#include "obstacle.h"
#include "collision_rect.h"
#include "projectile.h"

obstacle_t* get_obstacle(int16_t x, int16_t y, int16_t hp, uint16_t dir){
    obstacle_t* self = my_malloc(sizeof(obstacle_t));
    self->owner = NO_OWNER;

    self->init = obstacle_init;
    self->set_location = obstacle_set_location;
    self->draw = obstacle_draw;
    self->update = obstacle_update;
    self->free = free_obstacle;
    self->damage = obstacle_damage;

    self->init(self, x, y, hp, dir);

    return self;
}

int16_t obstacle_damage(struct obstacle* self, int16_t projectile_type) {
    int16_t ret = EXIT_SUCCESS;
    int16_t damage = 1;
    if (projectile_type == PROJECTILE_MEDIUM) {
        damage = 2;
    } else if (projectile_type == PROJECTILE_BIG) {
        damage = 3;
    }

    if (self == NULL){
        error("obstacle null in player draw");
        ret = OBSTACLE_NULL;
    } else if (self->hp > 0) {
        self->hp -= damage;
    }
 
}

void free_obstacle(struct obstacle* self) {
    free(self->collision_rect);
    free(self);
}

int16_t obstacle_init(struct obstacle* self, int16_t x, int16_t y, int16_t hp, uint16_t dir){
    int16_t ret = EXIT_SUCCESS;
    
    self->hp = hp;
    self->move_dir = dir;

    if(x < 0 || y < 0 || x > 480 || y > 360){
        ret = OBSTACLE_INVALID_LOCATION;
    } else {
        self->x = x;
        self->y = y;

        // Initialize collision_rect
        self->collision_rect = my_malloc(sizeof(collision_rect_t));
        self->collision_rect->x = x;
        self->collision_rect->y = y + OBSTACLE_COL_OFFSET_Y;
        self->collision_rect->width = DEFAULT_PICTURE_SIZE * 3;
        self->collision_rect->height = (DEFAULT_PICTURE_SIZE / 3) * 2;
    }

    return ret;
}

int16_t obstacle_set_location(struct obstacle* self,int16_t x, int16_t y){
    int16_t ret = EXIT_SUCCESS;

    if(self->x != -1){
        // warn("obstacle location already set");
    }
    
    if(x < 0 || y < 0 || x > 480 || y > 360){
        ret = OBSTACLE_INVALID_LOCATION;
    } else {
        self->x = x;
        self->y = y;

        self->collision_rect->x = x;
        self->collision_rect->y = y + OBSTACLE_COL_OFFSET_Y;
    }

    return ret;
}

int16_t obstacle_draw(struct obstacle* self,struct frame_buffer* buffer){
    int16_t ret = EXIT_SUCCESS;

    if(!self || !buffer){
        ret = OBSTACLE_INVALID_INPUT;
    }else if(self->x == -1 || self->y == -1){
        warn("obstacle not inited");
        error("obstacle with not set location will not be drawn");
    } else {
        buffer->draw_obstacle(buffer,self->x,self->y);
    }
}

int16_t obstacle_update(struct obstacle* self,struct frame_buffer* buffer) {
    int16_t ret = EXIT_SUCCESS;

    if (self->move_dir == OBSTACLE_LEFT) {
        self->set_location(self, self->x - DISTANCE_INCREMENT, self->y);
    } else if (self->move_dir == OBSTACLE_RIGHT) {
        self->set_location(self, self->x + DISTANCE_INCREMENT, self->y);
    }

    if (self->hp <= 0 || self->y <= 0 || self->y > SCREEN_HEIGHT - self->collision_rect->height || self->x > SCREEN_WIDTH - self->collision_rect->width || self->x <= 0) {
        ret = OBSTACLE_DESTROYED;
        self->free(self);
    } else {
        self->draw(self, buffer);
    }

    return ret;
}

int16_t sort_obstacles(obstacle_t** obstacles, const int16_t last_obstacle_index) {
    int16_t idx = 0;
    for(int16_t i;i < last_obstacle_index;++i){
        while(obstacles[idx] != NULL){
            if(idx >= last_obstacle_index) {
                return idx;
            }
            ++idx;
        }

        if(obstacles[i] != NULL && i > idx){
            obstacles[idx] = obstacles[i];
            obstacles[i] = NULL; 
        }
    }

    return idx;
}

bool obstacle_set_cooldown(int16_t* cooldown, uint16_t cycle) {
    *cooldown = *cooldown - cycle;
    if (*cooldown <= 0) {
        int r = rand() % OBSTACLE_COOLDOWN;
        *cooldown = OBSTACLE_COOLDOWN - r;
        return true;
    }

    return false;
}