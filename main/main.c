/*******************************************************************
    Main file for this application
    authors: Pelech Ondrej, Pham Thi Thien Trang 
    date:  Mai 2023
*******************************************************************/

#define _POSIX_C_SOURCE 200112L

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <time.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "serialize_lock.h"

#include "utils.h"
#include "draw.h"
#include "knobs.h"
#include "led_rgb.h"
#include "led_line.h"
#include "enemy.h"
#include "player.h"
#include "projectile.h"
#include "obstacle.h"
#include "collision.h"
#include "text.h"


#define FBUF_DTYPE uint16_t

#define FB_BLUE_MAX 0x1f
#define FB_GREEN_MAX 0x3f
#define FB_RED_MAX 0x1f

#define RGB(red, green, blue) (((red) & 0x1f) << 11 | ((green) & 0x3f) << 5 | ((blue) & 0x1f))
#define R(color) (((color) >> 11) & 0x1f)
#define G(color) (((color) >> 5) & 0x3f)
#define B(color) (((color) >> 0) & 0x1f)

#define FB_BLUE (RGB(0, 0, FB_BLUE_MAX))


int main(int argc, char *argv[]) {

    /* Serialize execution of applications */
    /* Try to acquire lock the first */
    printf("Hello world\n");
    if (serialize_lock(1) <= 0) {
        printf("System is occupied\n");
        if (1) {
            printf("Waitting\n");
            /* Wait till application holding lock releases it or exits */
            serialize_lock(0);
        }
    }

    frame_buffer_t* frame_buffer = my_malloc(sizeof(frame_buffer_t));
    int16_t ret = frame_buffer_init(frame_buffer, SCREEN_HEIGHT, SCREEN_WIDTH);
    if (ret != EXIT_SUCCESS) {
        printf("Frame buffer init error\n");
        exit(1);
    }
    
    void* spiled_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);

    // welcome text draw
    text_t* welcome_text = get_text("Welcome !!!");
    text_t* end_game_text;
    welcome_text->draw(welcome_text,frame_buffer,100,130);
    frame_buffer->draw(frame_buffer);
    frame_buffer->null_buffer(frame_buffer);

    sleep(4);

    // INITIALISATION
     bool victory = false;

    // ENEMIES
    int enemy_row = 3;
    int enemy_col = 7;
    int count_enemies = enemy_row * enemy_col;

    enemy_t** enemies = my_malloc(count_enemies * sizeof(enemy_t*));
    int num_enemy = 0;
    for (int i = 0; i < enemy_row; i++) {
        for (int j = 0; j < enemy_col; j ++) {
            enemies[num_enemy] = get_enemy(j * ENEMY_DISTANCE, i * DEFAULT_PICTURE_SIZE * 3, 3); // set the enemy distance
            num_enemy++;
        }
    }

    // PROJECTILES
    int num_projectiles = 0;
    int max_projectiles = 50;
    projectile_t** projectiles = my_malloc(max_projectiles * sizeof(projectile_t*));

    // OBSTACLES
    int num_obstacles = 0;
    int max_obstacles = 3;
    obstacle_t** obstacles = my_malloc(max_obstacles * sizeof(obstacle_t*));
    int obstacle_cooldown = rand() % OBSTACLE_COOLDOWN;
        
    // PLAYER
    player_t* player = get_player(SCREEN_WIDTH / 2 - DEFAULT_PICTURE_SIZE*3, SCREEN_HEIGHT - DEFAULT_PICTURE_SIZE*4, 100);
    set_ammo_count(spiled_base, 0);
    update_health_rgb1(spiled_base, player->max_hp, player->hp);
    LED_rgb2(spiled_base, rgb_to_int(255, 0, 255));

    uint32_t prev_knob_value = knobs(spiled_base);
    uint32_t prev_red = (prev_knob_value >> 16) & 0x1F;
    uint32_t prev_green = (prev_knob_value >> 10) & 0x3F;
    uint32_t prev_blue = prev_knob_value & 0x1F;

    while(1) {
        
        // handle knobs
        uint32_t knob_value = knobs(spiled_base);
        uint32_t knob_state = buttons(spiled_base);
        handle_knob_movement(knob_value, prev_knob_value, &prev_red, &prev_blue, &prev_green, player, frame_buffer);
        prev_knob_value = knob_value;
        handle_knob_buttons(knob_state, player, frame_buffer, projectiles, &num_projectiles, max_projectiles, spiled_base);
        
        player->draw(player, frame_buffer);
        update_health_rgb1(spiled_base, player->max_hp, player->hp);

        for (int i = 0; i < num_enemy; i++) {
            enemy_t* enemy = enemies[i];
            if ( enemy != NULL) {
                int ret = enemy->update(enemy, frame_buffer, 1);
                if (enemy->update_cooldown(enemy, 1) && num_projectiles + 1 < max_projectiles) {
                    projectile_t* new_projectile = enemy->shoot(enemy, frame_buffer);
                    if (new_projectile != NULL) {
                        projectiles[num_projectiles] = new_projectile;
                        num_projectiles = num_projectiles + 1;
                    }
                }
                if (ret == ENEMY_DEAD) {
                    enemies[i] = NULL;
                    count_enemies--;
                    player->heal(player, 10);
                    player->add_ammo(player, 40);
                    float used_ammo = (float)(player->max_ammo - player->ammo)/100 * 32;
                    set_ammo_count(spiled_base, (int)used_ammo);
                }
            }
        }

        for (int i = 0; i < num_projectiles; i++) {
            projectile_t* projectile = projectiles[i];
            if (projectile != NULL) {
                int ret = projectile->update_location(projectile, frame_buffer);
                if (ret == SCREEN_END) {
                    projectiles[i] = NULL;
                }
            }
        }

        num_projectiles = sort_projectiles(projectiles, max_projectiles);
        
        if (obstacle_set_cooldown(&obstacle_cooldown, 1) && num_obstacles + 1 < max_obstacles) {
            int16_t x = SCREEN_WIDTH - DEFAULT_PICTURE_SIZE*3;
            int16_t y = DEFAULT_PICTURE_SIZE * 3 * 3 + rand() % 100;
            int16_t dir = rand() % 2;
            if (dir == RIGHT) {
                x = 2;
            }
            obstacle_t* new_obstacle = get_obstacle(x, y, 10, dir);
            if (new_obstacle != NULL) {
                obstacles[num_obstacles] = new_obstacle;
                num_obstacles = num_obstacles + 1;
            }
        }


        for (int i = 0; i < num_obstacles; i++) {
            obstacle_t* obstacle = obstacles[i];
            if (obstacle != NULL) {
                int ret = obstacle->update(obstacle, frame_buffer);
                if (ret == OBSTACLE_DESTROYED) {
                    obstacles[i] = NULL;
                }
            }
        }
     
        num_obstacles = sort_obstacles(obstacles, max_obstacles);
 
        // check collision
        check_collisions(player, enemies, obstacles, num_obstacles, max_obstacles, projectiles, num_projectiles, max_projectiles, num_enemy);
     
        
        frame_buffer->draw(frame_buffer);
        frame_buffer->null_buffer(frame_buffer);

        if (count_enemies <= 0) {
            victory = true;
            break;
        }

        bool has_player_projectille = false;
        for (int i = 0; i < max_projectiles; i++) {
            if (projectiles[i] != NULL && projectiles[i]->owner == PLAYER) {
                has_player_projectille = true;
            } 
        }

        if (player->hp <= 0 || (player->ammo < PROJECTILE_SMALL_COST && !has_player_projectille)) {
            victory = false;
            break;
        }
    }

    if(victory) {
        end_game_text = get_text("YOU WON !!!");
    } else {
        end_game_text = get_text("GAME OVER !!!");
    }

    frame_buffer->null_buffer(frame_buffer);
    end_game_text->draw(end_game_text,frame_buffer,80,130);
    frame_buffer->draw(frame_buffer);

  // FREE OBJECTS
  info("free");
  debug("free projectiles");
  for (int i = 0; i < max_projectiles; i ++) {
    if (projectiles[i] != NULL)
      projectiles[i]->free(projectiles[i]);
  }
  free(projectiles);
  debug("free enemies");
  for (int i = 0; i < num_enemy; i ++) {
    if (enemies[i] != NULL)
      enemies[i]->free(enemies[i]);
  }
  free(enemies);

    debug("free obstacles");
    for (int i = 0; i < max_obstacles; i ++) {
        if (obstacles[i] != NULL)
            obstacles[i]->free(obstacles[i]);
    }
    free(obstacles);
    debug("free player");
    player->free(player);
    debug("free buffer");
    frame_buffer->free(frame_buffer);

//   debug("free text");
//   welcome_text->free(welcome_text);
//   debug("free end text");
//   end_game_text->free(end_game_text);

  printf("Goodbye world\n");

    serialize_unlock();

    return 0;
}
