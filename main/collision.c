/*******************************************************************
    File representing collisions between objects
    authors: Pelech Ondrej, Pham Thi Thien Trang 
    date:  May 2023
 *******************************************************************/

#include <stdint.h>

#include "enemy.h"
#include "player.h"
#include "projectile.h"
#include "obstacle.h"
#include "collision_rect.h"


// checks for object collisions of exact types
void check_collisions(player_t* player, enemy_t** enemies, obstacle_t** obstacles, int num_obstacles, int max_obstacles, projectile_t** projectiles,
                    int num_projectiles, int max_projectiles, int num_enemy) {
    
    check_collision_with_projectiles(player, enemies, obstacles, num_obstacles, max_obstacles, projectiles, num_projectiles, max_projectiles, num_enemy);
    
    check_player_collision_with_obstacles(player, obstacles, num_obstacles, max_obstacles);
}

// checks for collisions with projectiles
void check_collision_with_projectiles(player_t* player, enemy_t** enemies, obstacle_t** obstacles, int num_obstacles, int max_obstacles, projectile_t** projectiles,
                    int num_projectiles, int max_projectiles, int num_enemy) {
    for (int i = 0; i < num_projectiles; i++) {
        projectile_t* projectile = projectiles[i];
        if (projectile != NULL) {
            if (projectile->owner == PLAYER) {
                for (int j = 0; j < num_enemy; j++) {
                enemy_t* enemy = enemies[j];
                    if (enemy != NULL) {
                        if (check_collision_rect(enemy->collision_rect, projectile->collision_rect)) {
                        enemy->damage(enemy, projectile->type);
                        projectile->free(projectile);
                        projectiles[i] = NULL;
                        break;
                        }
                    }
                }
            }
        
            if (check_collision_rect(player->collision_rect, projectile->collision_rect)) {
                player->damage(player);
                projectile->free(projectile);
                projectiles[i] = NULL;
                break;
            }
        }
     
        for (int k = 0; k < num_obstacles; k++) {
            obstacle_t* obstacle = obstacles[k];
            if (obstacle != NULL) {
                if (check_collision_rect(obstacle->collision_rect, projectile->collision_rect)) {
                    obstacle->damage(obstacle, projectile->type);
                    projectile->free(projectile);
                    projectiles[i] = NULL;
                    break;

                }
            }
        }

        sort_obstacles(obstacles, max_obstacles);
    }

    sort_projectiles(projectiles, max_projectiles);
}

// checks for collisions with obstacles
void check_player_collision_with_obstacles(player_t* player, obstacle_t** obstacles, int num_obstacles, int max_obstacles) {
    for (int i = 0; i < num_obstacles; i++) {
        obstacle_t* obstacle = obstacles[i];
        if (obstacle != NULL) {
            if (check_collision_rect(player->collision_rect, obstacle->collision_rect)) {
                player->damage(player);
            }
        }
    }
}