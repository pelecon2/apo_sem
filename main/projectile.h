/*******************************************************************
    Header for projectile.c
    File representing projectile
    authors: Pelech Ondrej, Pham Thi Thien Trang 
    date:  May 2023
 *******************************************************************/

#ifndef __PROJECTILE_H__
#define __PROJECTILE_H__

#ifndef __PROJECTILE_TYPES__
#define __PROJECTILE_TYPES__

#define PROJECTILE_SMALL 1
#define PROJECTILE_MEDIUM 2
#define PROJECTILE_BIG 3

#define PROJECTILE_SMALL_COST 10
#define PROJECTILE_MEDIUM_COST 25
#define PROJECTILE_BIG_COST 50

#define PROJECTILE_SIZE 4

#endif

#define PROJECTILE_DISTANCE_INCREMENT 5

#include <stdlib.h>
#include "draw.h"
#include "collision_rect.h"

//projectile ERROR
enum {INVALID_LOCATION = 301,INVALID_INPUT,INVALID_PROJECTILE_TYPE,INVALID_OWNER};

//projectile 
enum {SCREEN_END = 30};

#ifndef __OWNERSHIP__
#define __OWNERSHIP__

#define PLAYER 'p'
#define ENEMY 'e'
#define NO_OWNER 'o'

#endif

typedef struct projectile{
    uint16_t x;
    uint16_t y;
    uint16_t type;

    char owner;

    collision_rect_t* collision_rect;

    int16_t (*init)(struct projectile* self,char plr,int16_t x, int16_t y,int16_t type);

    // draws projectile
    // Method projectile_draw
    int16_t (*draw)(struct projectile* self,struct frame_buffer* buffer);

    // updates projectile
    // Method projectile_update_location
    int16_t (*update_location)(struct projectile* self,struct frame_buffer* buffer);
    
    // sets location of buffer
    // Method projectile_set_location
    int16_t (*set_location)(struct projectile* self,int16_t x,int16_t y);

    // Method free_projectile
    void (*free)(struct projectile* self);
} projectile_t;


/** 
* creates projectile_t
* returns EXIT_SUCCESS on succes PROJECILE_ERROR otherwise
**/
projectile_t* get_projectile(char plr,int16_t x, int16_t y, int16_t type);

/** 
* inits projectile_t
* returns EXIT_SUCCESS on succes PROJECILE_ERROR otherwise
**/
int16_t projectile_init(struct projectile* self,char plr,int16_t x, int16_t y,int16_t type);

/** 
* sets projectile to locaion
* returns EXIT_SUCCESS on succes PROJECILE_ERROR otherwise
**/
int16_t projectile_set_location(struct projectile* self,int16_t x,int16_t y);

/** 
* draws projectile to frame_buffer
* input param projectile_t - projectile to be updated, framebuffer
* returns EXIT_SUCCESS on succes PROJECILE_ERROR otherwise
**/
int16_t projectile_draw(struct projectile* self,struct frame_buffer* buffer);

/** 
* adds movement increment to projectle_t and daws projectile in to buffer
* input param projectile_t - projectile to be updated, framebuffer
* returns EXIT_SUCCESS on succes PROJECILE_ERROR otherwise
**/
int16_t projectile_update_location(struct projectile* self,struct frame_buffer* buffer);
int16_t sort_projectiles(projectile_t** projectiles, const int16_t last_projectile_index);
void free_projectile(struct projectile* self);

#endif 
