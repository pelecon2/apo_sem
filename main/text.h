/*******************************************************************
    Header for text.c
    File filled with utility methods
    authors: Pelech Ondrej, Pham Thi Thien Trang 
    date:  Mai 2023
 *******************************************************************/

#ifndef __TEXT_H__
#define __TEXT_H__

#include <stdlib.h>
#include "draw.h"
#include "font_types.h"

#ifndef __TEXT_ERRORS__
#define __TEXT_ERRORS__

enum {TEXT_NULL = 600, TEXT_LOCATION};

#endif

typedef struct text{
    char* text;

    int16_t (*draw)(struct text* self,struct frame_buffer* buffer, int16_t x, int16_t y);
    void (*free)(struct text* self);
} text_t;

// returns struct of type text_t annd cllocates all function
text_t* get_text(const char* text);

// draws text to fram_buffer
int16_t text_draw(struct text* self,struct frame_buffer* buffer, int16_t x, int16_t y);

// frees text_t struct
void free_text(struct text* self);

#endif 
