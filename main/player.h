/*******************************************************************
    Header for player.c
    File representing player plane
    authors: Pelech Ondrej, Pham Thi Thien Trang 
    date:  May 2023
 *******************************************************************/

#ifndef __PLAYER__H__
#define __PLAYER__H__

#ifndef __OWNERSHIP__
#define __OWNERSHIP__

#define PLAYER 'p'
#define ENEMY 'e'
#define NO_OWNER 'o'

#endif

#ifndef __PLAYEROFFSET__
#define __PLAYEROFFSET__

#define PLAYER_OFFSET_X 0
#define PLAYER_OFFSET_Y 8

#endif

#ifndef __DIRECTIONS__
#define __DIRECTIONS__

#define LEFT 0
#define UP 1
#define RIGHT 2
#define DOWN 3

#endif

#ifndef __DASH_DISTANCE__
#define __DASH_DISTANCE__

#define USE_UNIVERSAL true
#define DASH_LEFT 30
#define DASH_UP 0
#define DASH_RIGHT 0
#define DASH_DOWN 30
#define DASH_UNIVERSAL 40 
#define DASH_COOLDOWN 2

#endif

#ifndef __DODGE_NUMS__
#define __DODGE_NUMS__

#define MAX_DOGES 1
#define DODGE_INCREMENT 1
#define DODGE_COOLDOWN 5

#endif

#include <stdlib.h>
#include "collision_rect.h"
#include "projectile.h"


#define PLAYER_DISTANCE_INCREMENT 10

//obstacle ERROR
enum {PLAYER_INVALID_LOCATION = 501,PLAYER_INVALID_INPUT, PLAYER_NULL};

typedef struct player{
    uint16_t x;
    uint16_t y;
    uint16_t max_hp;
    uint16_t hp;

    uint16_t ammo;
    uint16_t cd;
    
    uint16_t max_cd;
    uint16_t max_ammo;

    uint16_t projectile_type;

    uint16_t dodge_count;
    uint16_t move_dir;

    collision_rect_t* collision_rect;

    char owner;

    // inits player
    // Method player_init
    int16_t (*init)(struct player* self,int16_t x, int16_t y, int16_t hp);

    // draws player
    // Method player_draw
    int16_t (*draw)(struct player* self,struct frame_buffer* buffer);

    // updates location of player
    // Method player_update_location
    int16_t (*update_location)(struct player* self,struct frame_buffer* buffer, int x, int y);
    
    // sets location of player
    // Method player_set_location
    int16_t (*set_location)(struct player* self,int16_t x,int16_t y);

    // shoots player projectile
    // Method player_shoot
    projectile_t* (*shoot)(struct player* self,struct frame_buffer* buffer, int16_t projectile_type);
    
    // dashes player 
    // Method dash
    int16_t (*dash)(struct player* self,struct frame_buffer* buffer, uint16_t direction);
    int16_t (*damage)(struct player* self);
    int16_t (*imunity)(struct player* self);
    void (*projectile_scale_up)(struct player* self);
    void (*projectile_scale_down)(struct player* self);
    void (*heal)(struct player* self, int16_t heal);
    void (*add_ammo)(struct player* self, int16_t ammo);
    void (*free)(struct player* self);
} player_t;

/** 
* creates player_t
* input param x,y - initial location of player, initial hp
* returns player_t on succes NULL otherwise
**/
player_t* get_player(int16_t x, int16_t y,int16_t hp);

/** 
* inits player_t
* input param x,y - initial location of player, initial hp
* returns EXIT_SUCCESS on succes PROJECILE_ERROR otherwise
**/
int16_t player_init(struct player* self,int16_t x, int16_t y, int16_t hp);

/** 
* sets location of player_t
* input param x,y - location of player
* returns EXIT_SUCCESS on succes PROJECILE_ERROR otherwise
**/
int16_t player_set_location(struct player* self,int16_t x,int16_t y);

/** 
* draws player into framebuffer
* input param plaert_t - player, framebuffer
* returns EXIT_SUCCESS on succes PROJECILE_ERROR otherwise
**/
int16_t player_draw(struct player* self,struct frame_buffer* buffer);

/** 
* updtes player loction and draws the player
* input plaer_t - player, framebuffer, x,y - location
* returns EXIT_SUCCESS on succes PROJECILE_ERROR otherwise
**/
int16_t player_update_location(struct player* self,struct frame_buffer* buffer, int x, int y);

/** 
* shoots player loction and draws the player
* input param plyer_t - player, frame_buffer, projectile_type
* returns EXIT_SUCCESS on succes PROJECILE_ERROR otherwise
**/
projectile_t* player_shoot(struct player* self,struct frame_buffer* buffer, int16_t projectile_type);

/** 
* moves player in direction
* input param plyer_t - player, frame_buffer, drection
* returns EXIT_SUCCESS on succes PROJECILE_ERROR otherwise
**/
int16_t dash(struct player* self,struct frame_buffer* buffer, uint16_t direction);
int16_t damage(struct player* self);
int16_t imunity(struct player* self);
void projectile_scale_up(struct player* self);
void projectile_scale_down(struct player* self);
void player_heal(struct player* self, int16_t heal);
void player_add_ammo(struct player* self, int16_t ammo);

/** 
* frees player_t
* input param plyer_t - player
**/
void free_player(struct player* self);

#endif 