/*******************************************************************
  Method representing enemy
  authors: Pelech Ondrej, Pham Thi Thien Trang 
  date:  Mai 2023
 *******************************************************************/

#include <stdint.h>

#include "enemy.h"
#include "draw.h"
#include "projectile.h"
#include "collision_rect.h"

enemy_t* get_enemy(int16_t x, int16_t y, int16_t hp) {
    // mem allocations for enemy_t
    enemy_t* self = my_malloc(sizeof(enemy_t));

    // enemy methody assignment
    self->init = enemy_init;
    self->draw = enemy_draw;
    self->shoot = enemy_shoot;
    self->damage = damage_enemy;
    self->update = enemy_update;
    self->update_cooldown = enemy_set_cooldown;
    self->free = free_enemy;
    self->set_location = enemy_set_location;

    // enemy init
    self->init(self,x, y, hp);  

    return self;
}

int16_t enemy_init(struct enemy* self, int16_t x, int16_t y, int16_t hp) {
    int16_t ret = EXIT_SUCCESS;

    if(x < 0 || y < 0 || x > 480 || y > 360){
        ret = ENEMY_INVALID_LOCATION;
    } else if (self == NULL) {
        ret = ENEMY_NULL;
    } else {
        self->x = x;
        self->y = y;
        self->hp = hp;
        self->cooldown = ENEMY_COOLDOWN - rand() % 200;
        self->is_invincible = false;

        // Initialize collision_rect
        self->collision_rect = my_malloc(sizeof(collision_rect_t));
        self->collision_rect->x = x;
        self->collision_rect->y = y;
        self->collision_rect->width = DEFAULT_PICTURE_SIZE * 3;
        self->collision_rect->height = DEFAULT_PICTURE_SIZE * 3;
    }
    return ret;
}

void free_enemy(struct enemy* self){
    free(self->collision_rect);
    free(self);
}

int16_t enemy_set_location(struct enemy* self, int16_t x, int16_t y) {
    int16_t ret = EXIT_SUCCESS;

    if (self == NULL) {
        ret = ENEMY_NULL;
    } else if (x < 0 || y < 0 || x > 480 || y > 360) {
        ret = ENEMY_INVALID_LOCATION;
    } else {
        self->x = x;
        self->y = y;

        self->collision_rect->x = x;
        self->collision_rect->y = y;
    }

    return ret;
}

int16_t enemy_draw(struct enemy* self, struct frame_buffer* buffer) {
    int16_t ret = EXIT_SUCCESS;

    if (self == NULL) {
        ret = ENEMY_NULL;
    } else {
        ret = buffer->draw_enemy(buffer, self->x, self->y);
    }

    return ret;
}

projectile_t* enemy_shoot(struct enemy* self, struct frame_buffer* buffer) {
    uint16_t projectile_type = PROJECTILE_SMALL;
    projectile_t* projectile = get_projectile(ENEMY, self->x + DEFAULT_PICTURE_SIZE*3/2 , self->y + DEFAULT_PICTURE_SIZE*3, projectile_type);
    return projectile;
}

int16_t enemy_update(struct enemy* self, struct frame_buffer* buffer, uint16_t cycle) {
    int16_t ret = EXIT_SUCCESS;
    
    if (self->hp <= 0) {
        ret = ENEMY_DEAD;
        self->free(self);
    } else {
        ret = self->draw(self, buffer);
    }

    return ret;
}

bool enemy_set_cooldown(struct enemy* self, uint16_t cycle) {
    self->cooldown -= cycle;
    if(self->cooldown <= 0){
        int r = rand() % 200;
        self->cooldown = ENEMY_COOLDOWN - r;
        return true;
    }
    return false;
}

int16_t damage_enemy(struct enemy* self, int16_t projectile_type) {
    int16_t ret = EXIT_SUCCESS;
    int16_t damage = 1;
    if (projectile_type == PROJECTILE_MEDIUM) {
        damage = 2;
    } else if (projectile_type == PROJECTILE_BIG) {
        damage = 3;
    }

    if (self == NULL){
        error("enemy null in player draw");
        ret = ENEMY_NULL;
    } else if (self->hp > 0) {
        self->hp -= damage;
    }

    return ret;
}
