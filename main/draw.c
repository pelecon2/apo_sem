/*******************************************************************
    Class representing frame_buffer
    authors: Pelech Ondrej, Pham Thi Thien Trang 
    date:  Mai 2023
 *******************************************************************/
#define _POSIX_C_SOURCE 200112L

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "serialize_lock.h"

#include "utils.h"
#include "draw.h"
#include "text.h"
#include "letters.h"
#include "projectile.h"

// inits frame_buffer dimensions
// return 0 on success F_BUF error otherwise
int16_t frame_buffer_init(struct frame_buffer* self, FBUF_DTYPE h, FBUF_DTYPE w) {
    int16_t ret = EXIT_SUCCESS;
    if (w < 0 || h < 0 || w > 480 || h > 320) {
        ret = ERROR_WRONG_INIT_INPUT;
        error("wrong input in buffer init");
    } else if ( w != 480 || h != 320){
        warn("Unexpeted paramaters in frame_baffer_init");
        warn("Unexpeted behaivor may occur");
    }

    volatile void *lcdBaseAddr = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
    if (lcdBaseAddr == NULL) {
            printf("Counld not get rgb");
            exit(1);
    } else {
            self->lcdBaseAddr = lcdBaseAddr;
            parlcd_hx8357_init(lcdBaseAddr);
    }


    if(ret == EXIT_SUCCESS){
        // buffer varibles init
        self->height = h;
        self->width = w;
        self->buffer_length = w * h; // uint32_t so /2
        self->mem_size = w * h * sizeof(FBUF_DTYPE);
        self->buffer = my_malloc(self->mem_size);
        self->background_color = FB_BLACK;

        // buffer buffer init to background color
        for (int i = 0; i < self->buffer_length; ++i) {
            self->buffer[i] = (FBUF_DTYPE)self->background_color;
        }

        // buffer methods assignment
        self->draw = draw;
        self->draw_plane = draw_plane;
        self->draw_enemy = draw_enemy;
        self->draw_obstacle = draw_obstacle;
        self->draw_projectile = draw_projectile;
        self->color_background = color_background;
        self->null_buffer = null_buffer;
        self->draw_letter = draw_letter;
        self->free = free_buffer;

        info("draw.c : FRAME BUFFER INIT DONE");
    }

    return ret;
}

//sets whole buffer to color
//returns EXIT_SUCCESS on success, ERROR otherwise
int16_t color_background(struct frame_buffer* self,uint16_t color) {
    info("Coloring background...");
    int16_t ret = ERROR_INVALID_VALUES;
    if (!self) {
        ret = ERROR_INIT;
        error("draw.c : F_BUFF not inited"); 
    }else if(!self->buffer || !self->buffer_length){
        error("draw.c : frame buffer not initialized or contains invalid values");
        ret = ERROR_INVALID_VALUES;
    }else{
        if (self->lcdBaseAddr == NULL) {
            error("draw.c : could not access lcdBaseAddr. Exiting ..."); 
            exit(ERROR_ADDRESS_ACCESS);
        }

        for (int i = 0; i < self->buffer_length; ++i) {
            self->buffer[i] = (FBUF_DTYPE)color;
        }
    }
    return ret;
}

//draws buffer from framebuffer on display MZ_APO
//returns EXIT_SUCCESS on success, ERROR otherwise
int16_t draw(struct frame_buffer* self) {
    info("Drawing buffer to screen...");
    int16_t ret = 0;
    if (!self) {
        ret = ERROR_INIT;
        error("draw.c : F_BUFF not inited"); 
    } else {
        *(volatile uint16_t *)(self->lcdBaseAddr + PARLCD_REG_CMD_o) = 0x2c;
        for (int i = 0; i < self->buffer_length; ++i) {
            parlcd_write_data(self->lcdBaseAddr, self->buffer[i]);
        }
    }

    return ret;
}

//set plane to buffer to location x - horizontal, y - vertical
//returns EXIT_SUCCESS on success, ERROR otherwise
int16_t draw_plane(struct frame_buffer* self, const int x, const int y) {
    info("Drawing plane to buffer...");
    uint16_t ret = EXIT_SUCCESS;

    if (!self) {
        ret = ERROR_INIT;
        error("draw.c : F_BUFF not inited"); 
    }else if(!self->buffer){
        error("draw.c : frame buffer not initialized or contains invalid values");
        ret = ERROR_INVALID_VALUES;
    }

    volatile uint16_t plane_size = DEFAULT_PICTURE_SIZE;
    uint16_t plane_sprite[] = {
        0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
        0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
        0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0
    };

    uint16_t multipliactor = 3;
    uint16_t picture[DEFAULT_PICTURE_SIZE*DEFAULT_PICTURE_SIZE*9];
    multiply_buffer(picture,plane_sprite,plane_size,multipliactor);

    int pic_size = plane_size * multipliactor;

    for (int i = 0; i < pic_size*pic_size; ++i) {
        uint32_t col = i % pic_size;
        uint32_t row = i / pic_size; 
        int fb_index = ((y + row) * self->width) + (x + col);
        self->buffer[fb_index] = picture[i] != 0 ? FB_GREEN : (FBUF_DTYPE)self->background_color;
    }

    return ret;
}

//set enemy to buffer to location x - horizontal, y - vertical
//returns EXIT_SUCCESS on success, ERROR otherwise
int16_t draw_enemy(struct frame_buffer* self, const int x, const int y){
    info("Drawing enemy to buffer...");
    uint16_t ret = EXIT_SUCCESS;

    if (!self) {
        ret = ERROR_INIT;
        error("draw.c : F_BUFF not inited"); 
    }else if(!self->buffer){
        error("draw.c : frame buffer not initialized or contains invalid values");
        ret = ERROR_INVALID_VALUES;
    }
    

    volatile uint16_t enemy_size = DEFAULT_PICTURE_SIZE;
    uint16_t enemy_sprite[] = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0,
        0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0,
        0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0,
        1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 0,
        0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1,
        0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0,
        0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0,
        0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    };

    uint16_t multipliactor = 3;
    uint16_t picture[DEFAULT_PICTURE_SIZE*DEFAULT_PICTURE_SIZE*9];
    multiply_buffer(picture,enemy_sprite,enemy_size,multipliactor);

    int pic_size = enemy_size * multipliactor;

    for (int i = 0; i < pic_size*pic_size; ++i) {


        uint32_t col = i % pic_size;
        uint32_t row = i / pic_size; 
        int fb_index = ((y + row) * self->width) + (x + col);
        self->buffer[fb_index] = picture[i] != 0 ? FB_RED : (FBUF_DTYPE)self->background_color;
    }

    return ret;
}

//set letter to buffer to location x - horizontal, y - vertical
//returns EXIT_SUCCESS on success, ERROR otherwise
int16_t draw_letter(struct frame_buffer* self, const int x, const int y, char letter){
    info("Drawing letter to buffer...");
    uint16_t ret = EXIT_SUCCESS;

    if (!self) {
        ret = ERROR_INIT;
        error("draw.c : F_BUFF not inited"); 
    }else if(!self->buffer){
        error("draw.c : frame buffer not initialized or contains invalid values");
        ret = ERROR_INVALID_VALUES;
    }
    
    uint16_t* letter_sprite = get_letter(letter);
    
    uint16_t letter_size_w = 8;
    uint16_t letter_size_h = 16;

    uint16_t multipliactor = 3;
    uint16_t picture[letter_size_w*letter_size_h*9];

    multiply_letter(picture,letter_sprite,letter_size_w,letter_size_h,multipliactor);

    for(int i=0;i < letter_size_w * letter_size_h * multipliactor * multipliactor;++i){
        uint32_t col = i % (letter_size_w * multipliactor);
        uint32_t row = i / (letter_size_w * multipliactor); 

        int fb_index = ((y + row) * self->width) + (x + col);
        self->buffer[fb_index] = picture[i] != 0 ? FB_BLUE : (FBUF_DTYPE)self->background_color;
    }

    return ret;
}

//set obstacle to buffer to location x - horizontal, y - vertical
//returns EXIT_SUCCESS on success, ERROR otherwise
int16_t draw_obstacle(struct frame_buffer* self, const int x, const int y){
    info("Drawing obstacle to buffer...");
    uint16_t ret = EXIT_SUCCESS;

    if (!self) {
        ret = ERROR_INIT;
        error("draw.c : F_BUFF not inited"); 
    }else if(!self->buffer){
        error("draw.c : frame buffer not initialized or contains invalid values");
        ret = ERROR_INVALID_VALUES;
    }


    volatile uint16_t obstacle_size = DEFAULT_PICTURE_SIZE;
    uint16_t obstacle_sprite[] = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
        0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
        0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    };

    uint16_t multipliactor = 3;
    uint16_t picture[DEFAULT_PICTURE_SIZE*DEFAULT_PICTURE_SIZE*9];
    multiply_buffer(picture,obstacle_sprite,obstacle_size,multipliactor);

    int pic_size = obstacle_size * multipliactor;

    for (int i = 0; i < pic_size*pic_size; ++i) {
        uint32_t col = i % pic_size;
        uint32_t row = i / pic_size; 
        int fb_index = ((y + row) * self->width) + (x + col);
        // self->buffer[fb_index] = picture[i] != 0 ? FB_BLUE : (FBUF_DTYPE)self->background_color;
        self->buffer[fb_index] = picture[i] != 0 ? FB_BLUE : self->buffer[fb_index];
    }

    return ret;
}

//set projectile to buffer to location x - horizontal, y - vertical
//returns EXIT_SUCCESS on success, ERROR otherwise
//IS SAFE TO USE OVER OTHER PICTURES fce does not modyfi already drawn pictures
int16_t draw_projectile(struct frame_buffer* self, const int x, const int y, uint16_t type){
    info("Drawing projectile...");
    uint16_t ret = EXIT_SUCCESS;

    if (!self) {
        ret = ERROR_INIT;
        error("draw.c : F_BUFF not inited"); 
    }else if(!self->buffer){
        error("draw.c : frame buffer not initialized or contains invalid values");
        ret = ERROR_INVALID_VALUES;
    }

    volatile uint16_t obstacle_size = DEFAULT_PICTURE_SIZE;
    uint16_t obstacle_sprite[] = {
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    };


    uint16_t multipliactor = 3;
    if(type == PROJECTILE_SMALL){
        multipliactor = 1;
    } else if (type == PROJECTILE_MEDIUM){
        multipliactor = 2;
    }

    // uint16_t* picture =  my_malloc(DEFAULT_PICTURE_SIZE*DEFAULT_PICTURE_SIZE * multipliactor * multipliactor);
    uint16_t picture[DEFAULT_PICTURE_SIZE * DEFAULT_PICTURE_SIZE * multipliactor * multipliactor];
    multiply_buffer(picture, obstacle_sprite, obstacle_size, multipliactor);
    int pic_size = obstacle_size * multipliactor;

    for (int i = 0; i < pic_size*pic_size; ++i) {
        uint32_t col = i % pic_size;
        uint32_t row = i / pic_size; 
        int fb_index = ((y + row) * self->width) + (x + col);
        self->buffer[fb_index] = picture[i] != 0 ? FB_BLUE : self->buffer[fb_index];
    }

    // free(picture);
    
    return ret;

}

//sets buffer to buffer->background color
//makes new clean buffer
int16_t null_buffer(struct frame_buffer* self){
        info("Nulling buffer...");
        int16_t ret = EXIT_SUCCESS;
        if(self == NULL){
            ret = ERROR_BUFFER_NULL;
        }

        self->color_background(self,self->background_color);

        return ret;
}


//multiplies the size of the buffer with its contents
void multiply_buffer(uint16_t ret_buffer[],uint16_t buffer[],const uint16_t picture_size,uint16_t multipliactor){
    uint16_t buffer_size = picture_size*picture_size;

    int row_size = picture_size*multipliactor;

    for(int i=0;i<buffer_size;++i){


        for(int a=0;a<multipliactor;++a){
            int row = i/picture_size * multipliactor + a;

            for(int b=0;b<multipliactor;++b){

                int col = i % picture_size * multipliactor + b;

                ret_buffer[row * row_size + col] = buffer[i];
            }
        }
    }
}


void multiply_letter(uint16_t ret_buffer[],uint16_t buffer[],const uint16_t width, const uint16_t height,uint16_t multipliactor){
    uint16_t buffer_size = width*height;
    int row_size = width*multipliactor;

    for(int i=0;i<buffer_size;++i){
        for(int a=0;a<multipliactor;++a){
            int row = i/width * multipliactor + a;
            for(int b=0;b<multipliactor;++b){
                int col = i % width * multipliactor + b;
                ret_buffer[row * row_size + col] = buffer[i];
            }
        }
    }
}

void free_buffer(struct frame_buffer* self) {
    free(self->buffer);
    free(self);
}

/*

Picture canvas

0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0

*/
