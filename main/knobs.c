#include <stdint.h>
#include <time.h>

#include "mzapo_regs.h"
#include "knobs.h"
#include "player.h"
#include "projectile.h"
#include "enemy.h"
#include "draw.h"
#include "led_rgb.h"
#include "led_line.h"
#include "obstacle.h"

uint32_t knobs(void* spilled_base) {
    uint32_t *knobs = (spilled_base + SPILED_REG_KNOBS_8BIT_o);
    return *knobs;
}

uint32_t buttons(void* spilled_base) {
    uint32_t * buttons = (spilled_base + SPILED_REG_KBDRD_KNOBS_DIRECT_o );
    return *buttons;
}

void handle_knob_movement(uint32_t knob_value, uint32_t prev_knob_value, uint32_t* prev_red, uint32_t* prev_blue,
                        uint32_t* prev_green, player_t* player, frame_buffer_t* frame_buffer) {

    uint32_t red_knob = (knob_value >> 16) & 0x1F;
    uint32_t green_knob = (knob_value >> 10) & 0x3F;
    uint32_t blue_knob = knob_value & 0x1F;

    handle_red_knob_movement(red_knob, *prev_red, player, frame_buffer);
    if (red_knob % 4 == 0 && *prev_red % 4 == 0) *prev_red = red_knob;  
    
    handle_green_knob_movement(green_knob, *prev_green, player, frame_buffer);
    *prev_green = green_knob;

    handle_blue_knob_movement(blue_knob, *prev_blue, player);
     if (blue_knob % 4 == 0 && *prev_blue % 4 == 0) *prev_blue = blue_knob;
}

void handle_red_knob_movement(uint32_t red_knob, uint32_t prev_red, player_t* player, frame_buffer_t* frame_buffer) {
    
    if (red_knob == prev_red || red_knob % 4 != 0 || prev_red % 4 != 0) {
        return;
    }

    uint32_t diff = abs((int)red_knob - (int)prev_red);
    
    if (prev_red == 28 && red_knob == 0) {
        player->update_location(player, frame_buffer, player->x, player->y - PLAYER_DISTANCE_INCREMENT);
    } else if (prev_red == 0 && red_knob == 28) {
        player->update_location(player, frame_buffer, player->x, player->y + PLAYER_DISTANCE_INCREMENT);
    } else {
        if ((red_knob < prev_red) && diff != 31) {
            player->update_location(player, frame_buffer, player->x, player->y + PLAYER_DISTANCE_INCREMENT);
        } else {
            player->update_location(player, frame_buffer, player->x, player->y - PLAYER_DISTANCE_INCREMENT);
        }
    }
}

void handle_green_knob_movement(uint32_t green_knob, uint32_t prev_green, player_t* player, frame_buffer_t* frame_buffer) {
    
    if (green_knob == prev_green)
        return;

    uint32_t diff = abs((int)green_knob - (int)prev_green);

    if (green_knob < prev_green && diff != 63) {
        player->update_location(player, frame_buffer, player->x - PLAYER_DISTANCE_INCREMENT, player->y);
        player->move_dir = LEFT;
    } else {
        player->update_location(player, frame_buffer, player->x + PLAYER_DISTANCE_INCREMENT, player->y);
        player->move_dir = RIGHT;
    }
}

void handle_blue_knob_movement(uint32_t blue_knob, uint32_t prev_blue, player_t* player) {
    
    if (blue_knob == prev_blue || blue_knob % 4 != 0 || prev_blue % 4 != 0)
        return;

    uint32_t diff = abs((int)blue_knob - (int)prev_blue);

    if (prev_blue == 28 && blue_knob == 0) {
        player->projectile_scale_up(player);
    } else if (prev_blue == 0 && blue_knob == 28) {
        player->projectile_scale_down(player);
    } else {
        if (blue_knob < prev_blue && diff != 31) {
            player->projectile_scale_down(player);
        } else {
            player->projectile_scale_up(player);
        }
    }
}

void handle_knob_buttons(uint32_t knob_state, player_t* player, frame_buffer_t* frame_buffer, projectile_t** projectiles,
                        int* num_projectiles, int max_projectiles, void* spiled_base) {
    
    uint32_t knob_states = buttons(spiled_base);
    static int button_r_pressed = 0;
    static int button_g_pressed = 0;
    static int button_b_pressed = 0;
    static int can_dash = 1;
    static int can_immunity = 1;
    static time_t dash_start_time = 0;
    static time_t immunity_start_time = 0;

    time_t current_time = time(NULL);
    change_led_rgb2_based_on_powerups(spiled_base, current_time, dash_start_time, immunity_start_time, &can_dash, &can_immunity);

    handle_immunity_button(knob_state, &button_r_pressed, player, spiled_base, &immunity_start_time, &can_immunity);

    handle_dash_button(knob_state, &button_g_pressed, player, frame_buffer, spiled_base, &dash_start_time, &can_dash);

    handle_shoot_button(knob_states, &button_b_pressed, player, frame_buffer, projectiles, num_projectiles, max_projectiles, spiled_base);

    handle_button_released(knob_states, &button_r_pressed, &button_g_pressed, &button_b_pressed);
}

void handle_immunity_button(uint32_t knob_state, int* button_r_pressed, player_t* player, void* spiled_base,
                            time_t* immunity_start_time, int* can_immunity) {

    if ((knob_state & (1 << 24)) != 0 && !(*button_r_pressed)) {
                time_t current_time = time(NULL);
        if (*can_immunity) {
            player->imunity(player);
            *immunity_start_time = current_time;
            *can_immunity = 0;
            turn_off_immunity_color(spiled_base);
        }

        *button_r_pressed = 1;
    }
}

void handle_dash_button(uint32_t knob_state, int* button_g_pressed, player_t* player, frame_buffer_t* frame_buffer,
                        void* spiled_base, time_t* dash_start_time, int* can_dash) {

        if ((knob_state & (1 << 21)) != 0 && !(*button_g_pressed)) {
            if (*can_dash) {
                player->dash(player, frame_buffer, player->move_dir);
                *dash_start_time = time(NULL);
                turn_off_dash_color(spiled_base);
                *can_dash = 0;
            }

            *button_g_pressed = 1;
        }
}

void handle_shoot_button(uint32_t knob_states, int* button_b_pressed, player_t* player, frame_buffer_t* frame_buffer,
                        projectile_t** projectiles, int* num_projectiles, int max_projectiles, void* spiled_base) {

    if ((knob_states & (1 << 18)) != 0 && !(*button_b_pressed) && *num_projectiles + 1 < max_projectiles) {
        projectile_t* new_projectile = player->shoot(player, frame_buffer, player->projectile_type);
        if (new_projectile != NULL) {
            projectiles[*num_projectiles] = new_projectile;
            (*num_projectiles)++;

            float used_ammo = (float)(player->max_ammo - player->ammo) / 100 * 32;

            set_ammo_count(spiled_base, (int)used_ammo);
            *button_b_pressed = 1;
        }
    }
}

void handle_button_released(uint32_t knob_states, int* button_r_pressed, int* button_g_pressed, int* button_b_pressed) {
    if ((knob_states & (1 << 24)) == 0) {
        *button_r_pressed = 0;
    }

    if ((knob_states & (1 << 21)) == 0) {
        *button_g_pressed = 0;
    }

    if ((knob_states & (1 << 18)) == 0) {
        *button_b_pressed = 0;
    }
}