/*******************************************************************
    Header for player.c
    File representing obstacle
    authors: Pelech Ondrej, Pham Thi Thien Trang 
    date:  May 2023
 *******************************************************************/

#ifndef __OBSTACLE__H__
#define __OBSTACLE__H__

#ifndef __OWNERSHIP__
#define __OWNERSHIP__

#define PLAYER 'p'
#define ENEMY 'e'
#define NO_OWNER 'o'

#endif

#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include "collision_rect.h"
#include "projectile.h"

#define DISTANCE_INCREMENT 1.

#define OBSTACLE_LEFT 0
#define OBSTACLE_RIGHT 1

#define OBSTACLE_COOLDOWN 200

#define OBSTACLE_COL_OFFSET_X 0
#define OBSTACLE_COL_OFFSET_Y 8

//obstacle ERROR
enum {OBSTACLE_INVALID_LOCATION = 401, OBSTACLE_INVALID_INPUT, OBSTACLE_NULL};
enum {OBSTACLE_SCREEN_END = 40, OBSTACLE_DESTROYED};

typedef struct obstacle{
    uint16_t x;
    uint16_t y;
    uint16_t hp;
    uint16_t move_dir;

    char owner;

    collision_rect_t* collision_rect;

    int16_t (*init)(struct obstacle* self, int16_t x, int16_t y, int16_t hp, uint16_t dir);
    int16_t (*draw)(struct obstacle* self,struct frame_buffer* buffer);
    
    // updates obstacle
    // Method obstacle_update
    int16_t (*update)(struct obstacle* self,struct frame_buffer* buffer);
    
    // sets location of obstacle
    // Method obstacle_set_location
    int16_t (*set_location)(struct obstacle* self,int16_t x,int16_t y);
    int16_t (*damage)(struct obstacle* self, int16_t projectile_type);
    void (*free)(struct obstacle* self);
} obstacle_t;

obstacle_t* get_obstacle(int16_t x, int16_t y, int16_t hp, uint16_t dir);
int16_t obstacle_init(struct obstacle* self,int16_t x, int16_t y, int16_t hp, uint16_t dir);
int16_t obstacle_set_location(struct obstacle* self,int16_t x,int16_t y);

/** 
* inits obstacle_t
* input param obstacle_t - self, x,y - initial location of player, initial hp
* returns EXIT_SUCCESS on succes PROJECILE_ERROR otherwise
**/
int16_t obstacle_draw(struct obstacle* self,struct frame_buffer* buffer);

/** 
* updaes obstacle_t
* input param obstacle_t - self, framebffer
* returns EXIT_SUCCESS on succes PROJECILE_ERROR otherwise
**/
int16_t obstacle_update(struct obstacle* self,struct frame_buffer* buffer);
int16_t obstacle_damage(struct obstacle* self, int16_t projectile_type);
void free_obstacle(struct obstacle* self);
int16_t sort_obstacles(obstacle_t** obstacles, const int16_t last_obstacle_index);
bool obstacle_set_cooldown(int16_t* cooldown, uint16_t cycle);
#endif 