/*******************************************************************
    Class for accessing lights
    authors: Pelech Ondrej, Pham Thi Thien Trang 
    date:  Mai 2023
*******************************************************************/

#include <stdint.h>

#include "mzapo_regs.h"
#include "led_rgb.h"
#include "player.h"

// returns rgb as int value
uint32_t rgb_to_int(uint8_t r, uint8_t g, uint8_t b) {
    return r << 16 | g << 8 | b;
}

// sets color on rgb LED 1
void LED_rgb1(void* spiled_base, uint32_t color) {
    uint32_t *LED_rgb1 = (spiled_base + SPILED_REG_LED_RGB1_o);
    *LED_rgb1 = color;
}

// sets color on rgb LED 2
void LED_rgb2(void* spiled_base, uint32_t color) {
    uint32_t *LED_rgb1 = (spiled_base + SPILED_REG_LED_RGB2_o);
    *LED_rgb1 = color;
}

uint32_t get_led_rgb2_color(void* spiled_base) {
    uint32_t *LED_rgb1 = (spiled_base + SPILED_REG_LED_RGB2_o);
    return *LED_rgb1;

}

// shows health update on rgb LED 1
void update_health_rgb1(void* spiled_base, int max_hp, int hp) {
    uint32_t scaled_hp = (hp * 255 / max_hp - 50) < 0 ? 0 : hp * 255 / max_hp - 50;
    uint8_t r = 255 - scaled_hp;
    uint8_t g = scaled_hp;
    uint8_t b = 0;
    LED_rgb1(spiled_base, rgb_to_int(r, g, b));
}

void change_led_rgb2_based_on_powerups(void* spiled_base, uint32_t current_time, uint32_t dash_start_time, uint32_t immunity_start_time, int* can_dash, int* can_dodge) {
    if (current_time - dash_start_time >= DASH_COOLDOWN && !(*can_dash)) {
        uint32_t original_color = get_led_rgb2_color(spiled_base);
        *can_dash = 1;
        uint8_t r = (original_color >> 16);
        uint8_t g = (original_color >> 8);
        uint8_t b = original_color;
        LED_rgb2(spiled_base, rgb_to_int(r, g, 255));
        
    }

     if (current_time - immunity_start_time >= DODGE_COOLDOWN && !(*can_dodge)) {
        *can_dodge = 1;
        uint32_t original_color = get_led_rgb2_color(spiled_base);
        uint8_t r = (original_color >> 16);
        uint8_t g = (original_color >> 8);
        uint8_t b = original_color;
        
        LED_rgb2(spiled_base, rgb_to_int(255, g, b));
    }
}

void turn_off_dash_color(void* spiled_base) {
    uint32_t original_color = get_led_rgb2_color(spiled_base);
    uint8_t r = (original_color >> 16);
    uint8_t g = (original_color >> 8);
    uint8_t b = 0;
    LED_rgb2(spiled_base, rgb_to_int(r, g, b));
}

void turn_off_immunity_color(void* spiled_base) {
    uint32_t original_color = get_led_rgb2_color(spiled_base);
    uint8_t r = 0;
    uint8_t g = (original_color >> 8);
    uint8_t b = original_color;
    LED_rgb2(spiled_base, rgb_to_int(r, g, b));
}
