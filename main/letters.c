#include <stdlib.h>
#include "letters.h"
#include "font_rom8x16.c"
#include "utils.h"
#include <stdio.h>

#define base_letter_width 8
#define base_letter_height 16

int16_t* get_letter(char letter){
    int index = (int)letter;

    int16_t* ret = my_malloc(8*16*sizeof(int16_t));

    for(int i=0;i<base_letter_height;++i){
        uint16_t line = rom8x16_bits[index*0x10 + i];
        for (int j = 0; j < base_letter_width; j++) {
            uint16_t bit = (line & 0x8000) >> 15;
            ret[i * base_letter_width + j] = bit;
            line <<= 1;
        }
    }

    return ret;
}
