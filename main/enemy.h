/*******************************************************************
    Header file for enemy.c
    Method representing enemy
    authors: Pelech Ondrej, Pham Thi Thien Trang 
    date:  Mai 2023
 *******************************************************************/

#ifndef __ENEMY_H__
#define __ENEMY_H__

#include <stdint.h>
#include <stdbool.h>

#include "projectile.h"
#include "draw.h"
#include "collision_rect.h"

#ifndef __ENEMY_COOLDOWN__
#define __ENEMY_COOLDOWN__

//defalut enemy cooldown
#define ENEMY_COOLDOWN  200

#define ENEMY_DISTANCE 70

#endif


#define OFFSET_X 80
#define OFFSET_Y 20

enum {ENEMY_INVALID_LOCATION = 601, ENEMY_NULL};
enum {ENEMY_DEAD = 60};

typedef struct enemy{
    uint16_t x;
    uint16_t y;

    int16_t hp;
    int16_t cooldown;
    bool is_invincible;

    collision_rect_t* collision_rect;

    // inits enemt with input parameters
    // Method enemy_init
    int16_t (*init)(struct enemy* self, int16_t x, int16_t y, int16_t hp);

    // draws enemy to buffer
    // Method enemy_draw
    int16_t (*draw)(struct enemy* self, struct frame_buffer* buffer);

    // set location of an enemy
    // Method enemy_set_location
    int16_t (*set_location)(struct enemy* self, int16_t x, int16_t y);

    // returns projectile with location coresponding enemy location
    // Method enemy_shoot
    projectile_t* (*shoot)(struct enemy* self, struct frame_buffer* buffer);

    int16_t (*damage)(struct enemy* self, int16_t damage);

    // updates enemy depending on cycle
    // Method enemy_update
    int16_t (*update)(struct enemy* self, struct frame_buffer* buffer,uint16_t cycyle);
    // updates enemy cooldown
    // Method enemy_set_cooldown
    bool (*update_cooldown)(struct enemy* self, uint16_t cycyle);

    // frees enemt struct
    // Method free_enemy
    void (*free)(struct enemy* self);
} enemy_t;

/**
* allocates mem for enemy, assignes functions, and inits struct with input param
* input param enemy, int location x, int location y, int hp
* returs enemy_t on success NULL otherwise
**/
enemy_t* get_enemy(int16_t x, int16_t y, int16_t hp);

/**
* input param enemy, int location x, int location y, int hp
* returs EXIT_SUCCESS on success ERROR_ENEMY otherwise
**/
int16_t enemy_init(struct enemy* self,int16_t x, int16_t y, int16_t hp);

/**
* input param enemy, int location x, int location y
* returs EXIT_SUCCESS on success ERROR_ENEMY otherwise
**/
int16_t enemy_set_location(struct enemy* self, int16_t x, int16_t y);

/**
* calls frame_buffer->draw_enemy with enemy location param 
* input param enemy, frame_buffer
* returs EXIT_SUCCESS on success ERROR_ENEMY otherwise
**/
int16_t enemy_draw(struct enemy* self,struct frame_buffer* buffer);

/**
* cretes projectile coresponding to enemy location, allocating mem for projectile and drawing it to buffer
* input param enemy, frame_buffer
* returs EXIT_SUCCESS on success ERROR_ENEMY otherwise
**/
projectile_t* enemy_shoot(struct enemy* self, struct frame_buffer* buffer);

int16_t damage_enemy(struct enemy* self, int16_t damage);

/**
* updates cooldownon enemy shoots if enemy is out of cooldown and resets cooldown
* input param enemy, frame_buffer
* returs EXIT_SUCCESS on success ERROR_ENEMY otherwise
**/
int16_t enemy_update(struct enemy* self,struct frame_buffer* buffer,uint16_t cycle);

/**
* sets location of the enemy
* input param enemy, int cycle
* returs EXIT_SUCCESS on success ERROR_ENEMY otherwise
**/
bool enemy_set_cooldown(struct enemy* self, uint16_t cycyle);

/**
* frees enemy struct
**/
void free_enemy(struct enemy* self);

#endif 
